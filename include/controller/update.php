<?php
if(isset($_POST['updateid'], $_POST['updatetitle'], $_POST['updateartists'], $_POST['updaterelease']))
{
    $track = [
        'id' => $_POST['updateid'],
        'title' => $_POST['updatetitle'],
        'artists' => $_POST['updateartists'],
        'release' => $_POST['updaterelease'],
        'genre' => $_POST['updategenre'],
        'tags' => $_POST['updatetags'],
    ];
    
    try
    {
        $dbuser = (isset($_GET["section"])) ? setDBUser($_GET["section"]) : "guestprojet";
        $dbpassword = (isset($_GET["section"])) ? setDBPassword($_GET["section"]) : "mdp:GUEST";
        // récupération de la base de données
        $pdo = getPDO('root', '');//getPDO($dbuser, $dbpassword);
        // sélection des données avec filtre par tags
        updateTrack($pdo, $track);
        unset($_POST['updateid'], $_POST['updatetitle'], $_POST['updateartists'], $_POST['updaterelease'], $_POST['updategenre'], $_POST['updatetags']);
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
}
?>