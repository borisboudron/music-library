<?php
if(isset($_POST['deleteid']))
{
    $trackId = $_POST['deleteid'];
    try
    {
        $section = (isset($_GET["section"])) ? $_GET["section"] : "guest";
        echo $section;
        $dbuser = (isset($_GET["section"])) ? setDBUser($_GET["section"]) : "guestprojet";
        $dbpassword = (isset($_GET["section"])) ? setDBPassword($_GET["section"]) : "mdp:GUEST";
        // récupération de la base de données
        $pdo = getPDO('root', '');//getPDO($dbuser, $dbpassword);
        // suppression de la piste concernée
        deleteTrack($pdo, $trackId);
        unset($_POST['deleteid'], $_POST['deletetitle'], $_POST['deleteartists'], $_POST['deleterelease'], $_POST['deletegenre'], $_POST['deletetags']);
        
        header("Location:index.php?section=$section");
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
}
?>