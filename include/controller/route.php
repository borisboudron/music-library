<?php
if (isset($_GET['action']))
{
    switch ($_GET['action'])
    {
        case 'update':
            include("include/views/body/updateView.php");
            break;
        case 'delete':
            include("include/controller/delete.php");
            break;
    }
}
else
    include("include/views/body/mainView.php");
?>