<?php
if(isset($_POST['addtitle'], $_POST['addartists'], $_POST['addrelease']))
{
    $track = [
        'title' => $_POST['addtitle'],
        'artists' => $_POST['addartists'],
        'release' => $_POST['addrelease'],
        'genre' => $_POST['addgenre'],
        'tags' => $_POST['addtags'],
    ];
    
    try
    {
        $dbuser = (isset($_GET["section"])) ? setDBUser($_GET["section"]) : "guestprojet";
        $dbpassword = (isset($_GET["section"])) ? setDBPassword($_GET["section"]) : "mdp:GUEST";
        // récupération de la base de données
        $pdo = getPDO('root', '');//getPDO($dbuser, $dbpassword);
        // sélection des données avec filtre par tags
        addTrack($pdo, $track);
        unset($_POST['addtitle'], $_POST['addartists'], $_POST['addrelease'], $_POST['addgenre'], $_POST['addtags']);
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
}
?>