<?php
// si le nom existe via le formulaire 
if(isset($_POST["reguname"], $_POST["regemail"], $_POST["regpsw"], $_POST["regpsw2"]))
{
    // si le nom existe 
    if(($_POST["reguname"] != null)&&($_POST["regemail"] != null)&&($_POST["regpsw"] != null))
    {
        // comparer les passwords
        if ($_POST["regpsw"] == $_POST["regpsw2"])
        {
            try
            {
                //$pdo = getPDO('adminprojet', 'mdp:ADMIN'); // <--- changer l'accès
                $pdo = getPDO('root', ''); // <--- changer l'accès
                registerUser($pdo, $_POST["reguname"], $_POST["regemail"], password_hash($_POST["regpsw"], PASSWORD_DEFAULT));
                unset($_POST["reguname"], $_POST["regemail"], $_POST["regpsw"], $_POST["regpsw2"]);
            }
            catch(PDOException $e)
            {
                echo $e->getMessage();
            }
        }
        else
            echo "Les mots de passe ne correspondent pas.<br>";	
    }
    else
        echo "Des champs sont incomplets.<br>";	
}
?>