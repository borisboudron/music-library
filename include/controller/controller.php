<?php
include("include/models/model.php");

function explodeTags ($tags)
{
    $tagsarray = explode(';', $_POST['filtertags']);
    return $tagsarray;
}
function setDBUser ($section)
{
    switch ($section) 
    {
        case 'user':
            $dbuser = "userprojet";
            break;
        case 'admin':
            $dbuser = "adminprojet";
            break;
        case 'guest':
        default:
            $dbuser = "guestprojet";
            break;
    }
    return $dbuser;
}
function setDBPassword ($section)
{
    switch ($section) 
    {
        case 'user':
            $dbpassword = "mdp:USER";
            break;
        case 'admin':
            $dbpassword = "mdp:ADMIN";
            break;
        case 'guest':
        default:
            $dbpassword = "mdp:GUEST";
            break;
    }
    return $dbpassword;
}
function setLoginButton ($section)
{
    if (($section != 'user')&&($section != 'admin'))
        $response = '<button type="button" class="btn btn-secondary" onclick="document.getElementById(\'modal-login\').style.display=\'block\'">Se connecter</button>';
    else
        $response = '';
    return $response;
}
function setLoginModal ($section)
{
    $response = '';
    if (($section != 'user')&&($section != 'admin'))
    {?>
    <?php ob_start(); ?>
        <!-- Fenêtre modale d'identifiant -->
        <div id="modal-login" class="modal">
            <div class="modal-dialog" role="document">
                <form class="modal-content animate" action="#" method="post">
                    <!-- Titre et croix -->
                    <div class="modal-header justify-content-center">
                        <h5 class="modal-title">Se connecter</h5>
                        <button type="button" class="close" onclick="document.getElementById('modal-login').style.display='none'">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- Image, inputs et boutons -->
                    <div class="modal-body">                
                        <div class="imgcontainer">
                            <img src="img/portrait.png" alt="Avatar" class="avatar">
                        </div>
                        <label for="uname"><b>Identifiant *</b></label>
                        <input type="text" placeholder="Entrez votre identifiant" name="loguname" required>

                        <label for="psw"><b>Mot de passe *</b></label>
                        <input type="password" placeholder="Entrez votre mot de passe" name="logpsw" required>

                        <button class="btn btn-primary" type="submit">Connexion</button>
                        <label>
                        <input type="checkbox" checked="checked" name="remember"> Se souvenir de moi
                        </label>
                    </div>
                    <!-- Footer -->
                    <div class="modal-footer">
                        <span class="psw">Mot de passe <a href="#">oublié ?</a></span>
                        <button class="btn btn-outline-danger btn-sm" type="button" onclick="document.getElementById('modal-login').style.display='none'">Annuler</button>
                    </div>
                </form>
            </div>
        </div>       
        <?php
        $response = ob_get_clean();
        include ("include/controller/login.php");
    }
    return $response;
}
function setRegisterButton ($section)
{
    if (($section != 'user')&&($section != 'admin'))
        $response = '<button type="button" class="btn btn-secondary" onclick="document.getElementById(\'modal-register\').style.display=\'block\'">S\'inscrire</button>';
    else
        $response = '';
    return $response;
}
function setRegisterModal ($section)
{
    $response = '';
    if (($section != 'user')&&($section != 'admin'))
    {?>
        <?php ob_start(); ?>
        <!-- Fenêtre modale d'enregistrement -->
        <div id="modal-register" class="modal">
            <div class="modal-dialog" role="document">
                <form class="modal-content animate" action="#" method="post">
                    <!-- Titre et croix -->
                    <div class="modal-header justify-content-center">
                        <h5 class="modal-title">S'inscrire</h5>
                        <button type="button" class="close" onclick="document.getElementById('modal-register').style.display='none'">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- Image, inputs et boutons -->
                    <div class="modal-body">                
                        <div class="imgcontainer">
                            <img src="img/portrait.png" alt="Avatar" class="avatar">
                        </div>
                        <label for="uname"><b>Identifiant</b></label>
                        <input type="text" placeholder="Entrez un identifiant" name="reguname" required>
                        
                        <label for="email"><b>Email</b></label>
                        <input type="text" placeholder="Entrez votre email" name="regemail" required>

                        <label for="psw"><b>Mot de passe</b></label>
                        <input type="password" placeholder="Entrez un mot de passe" name="regpsw" required>
                        <input type="password" placeholder="Retapez ce mot de passe" name="regpsw2" required>
                            
                        <button class="btn btn-primary" type="submit">Inscription</button>
                    </div>
                    <!-- Footer -->
                    <div class="modal-footer">
                        <button class="btn btn-outline-danger btn-sm" type="button" onclick="document.getElementById('modal-register').style.display='none'">Annuler</button>
                    </div>
                </form>
            </div>
        </div>        
        <?php
        $response = ob_get_clean();
        include ("include/controller/register.php");
    }
    return $response;
}
function setLogoutButton ($section)
{
    if (($section == 'user')||($section == 'admin'))
        $response = '<span>Bienvenue !</span>  
                    <a href="include/controller/logout.php">
                    <button class="btn btn-secondary">Se déconnecter</button>
                    </a>';
    else
        $response = '';
    return $response;
}
function setAddButton ($section)
{
    if (($section == 'user')||($section == 'admin'))
        $response = '<button type="button" class="btn btn-secondary" onclick="document.getElementById(\'modal-add\').style.display=\'block\'">Ajouter une piste musicale</button>';
    else
        $response = '';
    return $response;
}
function setAddModal ($section)
{
    $response = '';
    if (($section == 'user')||($section == 'admin'))
    {
        ob_start(); ?>
        <!-- Fenêtre modale d'ajout -->
        <div id="modal-add" class="modal">
            <div class="modal-dialog" role="document">
                <form class="modal-content animate" action="#" method="post">
                    <!-- Titre et croix -->
                    <div class="modal-header justify-content-center">
                        <h5 class="modal-title">Nouvelle piste</h5>
                        <button type="button" class="close" onclick="document.getElementById('modal-add').style.display='none'">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- Image, inputs et boutons -->
                    <div class="modal-body">
                        <label for="title"><b>Titre *</b></label>
                        <input type="text" placeholder="Titre de la piste" name="addtitle" required>

                        <label for="artists"><b>Artistes *</b></label>
                        <input type="text" placeholder="Artistes de la pistes séparés par ';'" name="addartists" required>

                        <label for="release"><b>Date de sortie</b></label>
                        <input type="date" name="addrelease" required>

                        <label for="genre"><b>Genre</b></label>
                        <input type="text" placeholder="Genre de la piste" name="addgenre">

                        <label for="tags"><b>Tags</b></label>
                        <input type="text" placeholder="Tags associés à la piste" name="addtags">

                        <button class="btn btn-primary" type="submit">Valider</button>
                    </div>
                    <!-- Footer -->
                    <div class="modal-footer">
                        <button class="btn btn-outline-danger btn-sm" type="button" onclick="document.getElementById('modal-add').style.display='none'">Annuler</button>
                    </div>
                </form>
            </div>
        </div>        
        <?php
        $response = ob_get_clean();
        include ("include/controller/add.php");
    }
    return $response;
}
function setUpdate ($section)
{
    if ($section == 'admin')
    {
        include ("include/controller/update.php");
    }
}
?>