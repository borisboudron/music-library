<?php
function getPDO($dbuser, $dbpassword)
{
    $pdo = new PDO('mysql:host=localhost;dbname=musiclibrary;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    //$pdo = new PDO('mysql:host=localhost;dbname=musiclibrary;charset=utf8', $dbuser, $dbpassword, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    return $pdo;
}
function registerUser($pdo, $newuser, $newemail, $newpassword)
{
    $query = "INSERT into user
                (Username, Email, HashPwd) VALUES (:user, :email, :hashpwd)";
    
    $response = $pdo->prepare($query);
    $response->execute(array(
        ':user' => $newuser,
        ':email' => $newemail,
        ':hashpwd' => $newpassword,
    ));
}
function checkUser($pdo, $user, $password)
{
    $query = "SELECT Hashpwd from user
                WHERE Username = :user";
    
    $response = $pdo->prepare($query);
    $response->execute(array(
        ':user' => $user,
    ));
    $correcthashpwd = $response->fetch(PDO::FETCH_ASSOC);
    $correcthashpwd = $correcthashpwd['Hashpwd'];
    $corresponds = password_verify($password, $correcthashpwd);
    return $corresponds;
}
function getTrack($pdo, $trackId)
{
    // sélection des données avec filtre par tags
    $query = "SELECT inter.Id AS id, inter.Titre AS title, inter.Artistes AS artists, inter.DateDeSortie AS 'release', inter.Genre AS genre, GROUP_CONCAT(tag.Nom SEPARATOR ';') AS tags
    FROM (SELECT piste.Id AS Id, piste.Titre AS Titre, GROUP_CONCAT(artiste.Nom SEPARATOR ';') AS Artistes, piste.DateDeSortie AS 'DateDeSortie', piste.Genre AS Genre FROM artiste
            INNER JOIN pisteartistes ON artiste.Id = pisteartistes.ArtisteId
            INNER JOIN piste ON pisteartistes.PisteId = piste.Id
            GROUP BY piste.Id) AS inter
    INNER JOIN pistetags ON inter.id = pistetags.PisteId
    INNER JOIN tag ON pistetags.TagId = tag.Id
    GROUP BY inter.Id
    HAVING inter.Id = $trackId";
    // résultat de la requête
    $response = $pdo->query($query);
    // stockage des données
    $track = $response->fetch(PDO::FETCH_ASSOC);
    return $track;
}
function getFiltered($pdo, $startdate, $enddate, $tagsarray)
{
    // sélection des données avec filtre par dates
    $query = "SELECT inter.Id AS Id, inter.Titre AS Titre, inter.Artistes AS Artistes, inter.DateDeSortie AS 'Date de sortie', inter.Genre AS Genre, GROUP_CONCAT(tag.Nom SEPARATOR ';') AS Tags
                FROM (SELECT piste.Id AS Id, piste.Titre AS Titre, GROUP_CONCAT(artiste.Nom SEPARATOR ';') AS Artistes, piste.DateDeSortie AS 'DateDeSortie', piste.Genre AS Genre FROM artiste
                        INNER JOIN pisteartistes ON artiste.Id = pisteartistes.ArtisteId
                        INNER JOIN piste ON pisteartistes.PisteId = piste.Id
                        GROUP BY piste.Id) AS inter
                INNER JOIN pistetags ON inter.id = pistetags.PisteId
                INNER JOIN tag ON pistetags.TagId = tag.Id
                GROUP BY inter.Id
                HAVING (inter.DateDeSortie BETWEEN :startdate AND :enddate) AND ";
    // paramètres de date
    $parameters = array(
        ':startdate' => $startdate,
        ':enddate' => $enddate
    );
    // paramètres de tags et ajout dans la query
    $nbTags = count($tagsarray);
    if ($nbTags > 0)
        for ($i = 0; $i < $nbTags; $i++)
        {
            $query .= "(Titre LIKE :tag$i OR Artistes LIKE :tag$i OR Genre LIKE :tag$i OR Tags LIKE :tag$i)";
            $tag = ":tag$i";
            $parameters[$tag] = "%" . $tagsarray[$i] . "%";
            if ($i != $nbTags - 1)
                $query .= " AND ";
        }
    else
        $query .= "TRUE";
    // résultat de la requête
    $response = $pdo->prepare($query);
    $response->execute($parameters);
    // stockage des données
    $tracks = $response->fetchAll(PDO::FETCH_NUM);
    return $tracks;
}
function addTrack ($pdo, $track)
{
    // ajout d'une piste
    $query = "INSERT INTO piste
                (Titre, DateDeSortie, Genre) VALUES (:title, :release, :genre)";
    $response = $pdo->prepare($query);
    $response->execute(array(
        ':title' => $track['title'],
        ':release' => $track['release'],
        ':genre' => $track['genre'],
    ));

    // récupération de l'ID de la piste
    $query = "SELECT Id FROM piste
                WHERE Titre = :title AND DateDeSortie = :release AND Genre = :genre";
    $response = $pdo->prepare($query);
    $response->execute(array(
        ':title' => $track['title'],
        ':release' => $track['release'],
        ':genre' => $track['genre'],
    ));
    $exists = $response->fetch(PDO::FETCH_ASSOC);
    $trackId = $exists['Id'];

    // ajout des artistes qui n'existent pas encore
    $artistsarray = explode(';', $track['artists']);
    foreach ($artistsarray as $a)
    {
        // regarder si l'artiste existe déjà ou non
        $query = "SELECT Id FROM artiste
                    WHERE NOM = :artist";
        $response = $pdo->prepare($query);
        $response->execute(array(
            ':artist' => $a,
        ));
        if (!($exists = $response->fetch(PDO::FETCH_ASSOC)))
        {
            $query = "INSERT INTO artiste
                        (Nom) VALUES (:artist)";
            $response = $pdo->prepare($query);
            $response->execute(array(
                ':artist' => $a,
            ));
            $query = "SELECT Id FROM artiste
                        WHERE NOM = :artist";
            $response = $pdo->prepare($query);
            $response->execute(array(
                ':artist' => $a,
            ));
            $exists = $response->fetch(PDO::FETCH_ASSOC);
        }
        $artistId = $exists['Id'];
        $query = "INSERT INTO pisteartistes
                    (PisteId, ArtisteId) VALUES ('$trackId', '$artistId')";
        $response = $pdo->exec($query);
    }

    // ajout des tags qui n'existent pas encore
    $tagsarray = explode(';', $track['tags']);
    foreach ($tagsarray as $t)
    {
        // regarder si l'artiste existe déjà ou non
        $query = "SELECT Id FROM tag
                    WHERE NOM = :tag";
        $response = $pdo->prepare($query);
        $response->execute(array(
            ':tag' => $t,
        ));
        if (!($exists = $response->fetch()))
        {
            $query = "INSERT INTO tag
                        (Nom) VALUES (:tag)";
            $response = $pdo->prepare($query);
            $response->execute(array(
                ':tag' => $t,
            ));
            $query = "SELECT Id FROM tag
                        WHERE NOM = :tag";
            $response = $pdo->prepare($query);
            $response->execute(array(
                ':tag' => $t,
            ));
            $exists = $response->fetch(PDO::FETCH_ASSOC);
        }
        $tagId = $exists['Id'];
        $query = "INSERT INTO pistetags
                    (PisteId, TagId) VALUES ('$trackId', '$tagId')";
        $tags = $pdo->exec($query);
    }
}
function updateTrack ($pdo, $track)
{
    // récupération de l'ID de la piste
    $trackId = $track['id'];
    // suppression des anciens liens piste-artistes
    $query = "DELETE FROM pisteartistes
                WHERE PisteId = '$trackId'";
    $response = $pdo->exec($query);

    // suppression des anciens liens piste-tags
    $query = "DELETE FROM pistetags
                WHERE PisteId = '$trackId'";
    $response = $pdo->exec($query);

    // ajout des artistes qui n'existent pas encore
    $artistsarray = explode(';', $track['artists']);
    foreach ($artistsarray as $a)
    {
        // regarder si l'artiste existe déjà ou non
        $query = "SELECT Id FROM artiste
                    WHERE NOM = :artist";
        $response = $pdo->prepare($query);
        $response->execute(array(
            ':artist' => $a,
        ));
        if (!($exists = $response->fetch(PDO::FETCH_ASSOC)))
        {
            $query = "INSERT INTO artiste
                        (Nom) VALUES (:artist)";
            $response = $pdo->prepare($query);
            $response->execute(array(
                ':artist' => $a,
            ));
            $query = "SELECT Id FROM artiste
                        WHERE NOM = :artist";
            $response = $pdo->prepare($query);
            $response->execute(array(
                ':artist' => $a,
            ));
            $exists = $response->fetch(PDO::FETCH_ASSOC);
        }
        $artistId = $exists['Id'];
        $query = "INSERT INTO pisteartistes
                    (PisteId, ArtisteId) VALUES ('$trackId', '$artistId')";
        $response = $pdo->exec($query);
    }

    // ajout des tags qui n'existent pas encore
    $tagsarray = explode(';', $track['tags']);
    foreach ($tagsarray as $t)
    {
        // regarder si l'artiste existe déjà ou non
        $query = "SELECT Id FROM tag
                    WHERE NOM = :tag";
        $response = $pdo->prepare($query);
        $response->execute(array(
            ':tag' => $t,
        ));
        if (!($exists = $response->fetch()))
        {
            $query = "INSERT INTO tag
                        (Nom) VALUES (:tag)";
            $response = $pdo->prepare($query);
            $response->execute(array(
                ':tag' => $t,
            ));
            $query = "SELECT Id FROM tag
                        WHERE NOM = :tag";
            $response = $pdo->prepare($query);
            $response->execute(array(
                ':tag' => $t,
            ));
            $exists = $response->fetch(PDO::FETCH_ASSOC);
        }
        $tagId = $exists['Id'];
        $query = "INSERT INTO pistetags
                    (PisteId, TagId) VALUES ('$trackId', '$tagId')";
        $tags = $pdo->exec($query);
    }
    // update de la piste
    $query = "UPDATE piste
                SET Titre = :title, Genre = :genre, DateDeSortie = :release
                WHERE Id = :id";
    $response = $pdo->prepare($query);
    $response->execute(array(
        ':id' => $trackId,
        ':title' => $track['title'],
        ':release' => $track['release'],
        ':genre' => $track['genre'],
    ));
    
}
function deleteTrack ($pdo, $trackId)
{
    // suppression des liens piste-tags
    $query = "DELETE FROM pistetags
                WHERE PisteId = '$trackId'";
    $response = $pdo->exec($query);

    // suppression des liens piste-artistes
    $query = "DELETE FROM pisteartistes
                WHERE PisteId = '$trackId'";
    $response = $pdo->exec($query);

    // suppression dela piste
    $query = "DELETE FROM piste
                WHERE Id = '$trackId'";
    $response = $pdo->exec($query);
}
?>