<header>
    <h1>Trouvez l'inspiration</h1>
</header>
<div class="btn-group-horizontal headerbuttons">
    <?= $loginbutton ?>
    <?= $registerbutton ?>
    <?= $logoutbutton ?>
</div>
<?= $loginmodal ?>
<?= $registermodal ?>
<div class="container">
    <?= $contents ?>
</div>