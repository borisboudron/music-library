<?php
// définition de la section actuelle
$section = (isset($_GET["section"])) ? $_GET["section"] : 'guest';
// variables pour l'accès à la DB
$tagsconditions = "";
$startdate = "";
$enddate = "";

// variables d'affichage
$loginbutton = setLoginButton($section);
$loginmodal = setLoginModal($section);
$registerbutton = setRegisterButton($section);
$registermodal = setRegisterModal($section);
$logoutbutton = setLogoutButton($section);

// connexion avec la base de données avec PDO
$track = [];
try
{
    $dbuser = (isset($_GET["section"])) ? setDBUser($_GET["section"]) : "guestprojet";
    $dbpassword = (isset($_GET["section"])) ? setDBPassword($_GET["section"]) : "mdp:GUEST";
    // récupération de la base de données
    $pdo = getPDO('root', '');//getPDO($dbuser, $dbpassword);
    // sélection des données à update
    if (isset($_POST['updateid']))
        $track = getTrack($pdo, $_POST['updateid']);
}
catch(PDOException $e)
{
    echo $e->getMessage();
}
$tracks = [];
$nbTracks = count($tracks);
if ($nbTracks > 0)
    $nbColumns = count($tracks[0]);


ob_start(); ?>
<form class="modal-content animate" action="index.php?section=<?= $section ?>" method="post">
    <!-- Titre et croix -->
    <div class="modal-header justify-content-center">
        <h5 class="modal-title">Modifier la piste</h5>
    </div>
    <!-- Image, inputs et boutons -->
    <div class="modal-body">
        <input type="hidden" name="updateid" value="<?= $track['id']?>">
        
        <label for="title"><b>Titre *</b></label>
        <input type="text" placeholder="Titre de la piste" name="updatetitle" value="<?= $track['title']?>" required>

        <label for="artists"><b>Artistes *</b></label>
        <input type="text" placeholder="Artistes de la pistes séparés par ';'" name="updateartists" value="<?= $track['artists']?>" required>

        <label for="release"><b>Date de sortie</b></label>
        <input type="date" name="updaterelease" value="<?= $track['release']?>" required>

        <label for="genre"><b>Genre</b></label>
        <input type="text" placeholder="Genre de la piste" name="updategenre" value="<?= $track['genre']?>">

        <label for="tags"><b>Tags</b></label>
        <input type="text" placeholder="Tags associés à la piste" name="updatetags" value="<?= $track['tags']?>">

        <button class="btn btn-primary" type="submit">Valider</button>
    </div>
    <!-- Footer -->
    <div class="modal-footer">
        <button class="btn btn-outline-danger btn-sm" type="button" onclick="document.getElementById('modal-update').style.display='none'">Annuler</button>
    </div>
</form>
<?php
$update = ob_get_clean();
$contents = $update;
include("include/views/viewTemplate.php");
?>