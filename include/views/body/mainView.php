<?php
// définition de la section actuelle
$section = (isset($_GET["section"])) ? $_GET["section"] : 'guest';
// variables pour l'accès à la DB
$tagsarray = (isset($_POST['filtertags']) && $_POST['filtertags'] != "") ? explodeTags($_POST['filtertags']) : array();
$startdate = (isset($_POST['startdate']) && $_POST['startdate'] != "") ? $_POST['startdate'] : "0001-01-01";
$enddate = (isset($_POST['enddate']) && $_POST['enddate'] != "") ? $_POST['enddate'] : "9999-12-31";
// variables d'affichage
$loginbutton = setLoginButton($section);
$loginmodal = setLoginModal($section);
$registerbutton = setRegisterButton($section);
$registermodal = setRegisterModal($section);
$logoutbutton = setLogoutButton($section);
$addbutton = setAddButton($section);
ob_start(); ?>
<div id="loggedbuttons" class="d-flex align-items-center">
    <div><h4>Filtres :</h4></div>
    <div class="btn-group-horizontal">
        <?= $addbutton ?>
    </div>
</div>
<?php
$loggedbuttons = ob_get_clean();
// addModal contient l'ajout dans la DB et doit être défini avant le chargement de la DB dans $tracks
$addmodal = setAddModal($section);
// setUpdate contient l'update dans la DB et doit être défini avant le chargement de la DB dans $tracks
setUpdate($section);

// connexion avec la base de données avec PDO
$tracks = [];
try
{
    $dbuser = setDBUser($section);
    $dbpassword = setDBPassword($section);
    // récupération de la base de données
    $pdo = getPDO('root', '');//getPDO($dbuser, $dbpassword);
    // sélection des données avec filtre par tags
    $tracks = getFiltered($pdo, $startdate, $enddate, $tagsarray);
}
catch(PDOException $e)
{
    echo $e->getMessage();
}
$nbTracks = count($tracks);
if ($nbTracks > 0)
    $nbColumns = count($tracks[0]);
// Formulaire de filtre
ob_start(); ?>
<form action="#" method="post">
    <div>  
        <label for="datedebut">Date de sortie comprise entre :</label>
        <input class="form-control-sm" type="date" name="startdate" value="<?php if(isset($_POST['startdate'])) echo $_POST['startdate']?>">
        <label for="datefin"> et </label>
        <input class="form-control-sm" type="date" name="enddate" value="<?php if(isset($_POST['enddate'])) echo $_POST['enddate']?>">
    </div>
    <div class="input-group mb-2">  
        <input class="form-control form-control-sm" class="tags-input" type="text" name="filtertags" placeholder="Tags, séparés par ';'" value="<?php if(isset($_POST['filtertags'])) echo $_POST['filtertags']?>">
        <input class="btn btn-secondary btn-sm input-group-append" type="submit" value="Filtrer par tags">
    </div>
</form>
<?php
$filterform = ob_get_clean();
// table d'affichage
ob_start(); ?>
<table class="table table-hover">
    <tr class="table-primary">
        <?php
        if ($section == 'admin')
        {?>
        <th></th>
        <?php
        }?>
        <th scope="col">Titre</th>
        <th scope="col">Artistes</th>
        <th scope="col">Date de sortie</th>
        <th scope="col">Genre</th>
        <th scope="col">Tags</th>
    </tr>
    <?php
    for ($i = 0; $i < $nbTracks; $i++)
    {?>
        <tr class='<?=($i % 2 == 0) ? "table-secondary" : "table-dark"?>'>
        <?php
        if ($section == 'admin')
        {?>
        <td>
            <div class="btn-group" role="group">
                <form action="index.php?section=<?= $section ?>&action=update" method="post">
                    <input type="hidden" name="updateid" value="<?=$tracks[$i][0]?>">
                    <input type="image" src="img/icons8-edit-50.png" style="width:15px;height:15px;margin-right:10px;" alt="update">
                </form>
                <form action="index.php?section=<?= $section ?>&action=delete" method="post">
                    <input type="hidden" name="deleteid" value="<?=$tracks[$i][0]?>">
                    <input type="image" src="img/icons8-trash-can-50.png" style="width:15px;height:15px;" alt="delete">
                </form>
            </div>
        </td>
        <?php
        }
        for ($j = 1; $j < $nbColumns; $j++)
            echo "<td>" . $tracks[$i][$j] . "</td>";?>
        </tr>
    <?php
    }?>
</table>
<?php
$trackstable = ob_get_clean();

$contents = $loggedbuttons . $addmodal . $filterform . $trackstable;
include("include/views/viewTemplate.php");
?>