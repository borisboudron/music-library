<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 18-09-18
 * Time: 14:36
 */

namespace MusicLibrary\Repositories;
use MusicLibrary\Models\Artist;
use Toolbox\SimpleObjectRepository;

class ArtistRepository extends SimpleObjectRepository
{

    protected function getTableName()
    {
        return 'artiste';
    }

    protected function getPKBinding()
    {
        return ['Id' => 'id'];
    }

    protected function getEntityName()
    {
        return Artist::class;
}

    protected function getBindings()
    {
        return [
            "Nom" => "name",
        ];
    }
}