<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 18-09-18
 * Time: 14:38
 */

namespace MusicLibrary\Repositories;
use MusicLibrary\Models\Tag;
use Toolbox\SimpleObjectRepository;

class TagRepository extends SimpleObjectRepository
{
    protected function getTableName()
    {
        return 'tag';
    }

    protected function getPKBinding()
    {
        return ['Id' => 'id'];
    }

    protected function getEntityName()
    {
        return Tag::class;
    }

    protected function getBindings()
    {
        return [
            "Nom" => "name",
        ];
    }
}