<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 20-09-18
 * Time: 11:15
 */

namespace MusicLibrary\Repositories;


use MusicLibrary\Models\TrackArtists;
use PDO;
use Toolbox\BaseRepository;

class TrackArtistsRepository extends BaseRepository
{
    protected function getTableName()
    {
        return 'pisteartistes';
    }

    protected function getPKBinding()
    {
        return ['Id' => 'id'];
    }

    protected function getEntityName()
    {
        return TrackArtists::class;
    }

    protected function getBindings()
    {
        return [
            "PisteId" => "trackId",
            "ArtisteId" => "artistId",
        ];
    }

    public function getByTrackId($id)
    {
        $query = "SELECT * FROM pisteartistes WHERE pisteId = :id";
        $response = $this->pdo->prepare($query);
        $tab[":id"] = $id;
        $response->execute($tab);
        return $response->fetchAll(PDO::FETCH_CLASS, TrackArtists::class);
    }

}