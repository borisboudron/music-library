<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 18-09-18
 * Time: 13:15
 */

namespace MusicLibrary\Repositories;
use MusicLibrary\Models\Track;
use Toolbox\BaseRepository;

class TrackRepository extends BaseRepository
{

    protected function getTableName()
    {
        return 'piste';
    }

    protected function getPKBinding()
    {
        return ['Id' => 'id'];
    }

    protected function getEntityName()
    {
        return Track::class;
    }

    protected function getBindings()
    {
        return [
            "Titre" => "title",
            "DateDeSortie" => "releaseDate",
            "Genre" => "genre",
        ];
    }
}