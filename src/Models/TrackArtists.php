<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 20-09-18
 * Time: 11:10
 */

namespace MusicLibrary\Models;


use MusicLibrary\Repositories\ArtistRepository;

class TrackArtists
{
    private $id;
    private $trackId;
    private $artistId;
    private $artist;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return TrackArtists
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTrackId()
    {
        return $this->trackId;
    }

    /**
     * @param mixed $trackId
     * @return TrackArtists
     */
    public function setTrackId($trackId)
    {
        $this->trackId = $trackId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getArtistId()
    {
        return $this->artistId;
    }

    /**
     * @param mixed $artistId
     * @return TrackArtists
     */
    public function setArtistId($artistId)
    {
        $this->artistId = $artistId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getArtist()
    {
        if ($this->artist == null)
        {
            $repo = new ArtistRepository();
            $this->artist = $repo->getByID($this->artistId);
        }
        return $this->artist;
    }

    /**
     * @param mixed $artists
     * @return TrackArtists
     */
    public function setArtists($artists)
    {
        $this->artists = $artists;
        return $this;
    }
}