<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 18-09-18
 * Time: 12:55
 */

namespace MusicLibrary\Models;

use MusicLibrary\Repositories\ArtistRepository;
use MusicLibrary\Repositories\TagRepository;
use MusicLibrary\Repositories\TrackArtistsRepository;

class Track
{
    private $id;
    private $artists;
    private $title;
    private $releaseDate;
    private $genre;
    private $tags;
    private $trackArtists;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Track
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getArtists()
    {
        if ($this->artists == null)
        {
            $repo = new ArtistRepository();
            $this->artists = $repo->getList($this->id);
        }
        return $this->artists;
    }

    /**
     * @param mixed $artists
     * @return Track
     */
    public function setArtists($artists)
    {
        $this->artists = $artists;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Track
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReleaseDate()
    {
        return $this->releaseDate;
    }

    /**
     * @param mixed $releaseDate
     * @return Track
     */
    public function setReleaseDate($releaseDate)
    {
        $this->releaseDate = $releaseDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * @param mixed $genre
     * @return Track
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        if ($this->tags == null)
        {
            $repo = new TagRepository();
            $this->tags = $repo->getList($this->id);
        }
        return $this->tags;
    }

    /**
     * @param mixed $tags
     * @return Track
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTrackArtists()
    {
        if ($this->trackArtists == null)
        {
            $repo = new TrackArtistsRepository();
            $this->trackArtists = $repo->getByTrackId($this->id);
        }
        return $this->trackArtists;
    }

    /**
     * @param mixed $trackArtists
     */
    public function setTrackArtists($trackArtists)
    {
        $this->trackArtists = $trackArtists;
    }
}