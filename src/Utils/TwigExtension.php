<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 19-09-18
 * Time: 16:22
 */

namespace MusicLibrary\Utils;


class TwigExtension
{
    static function path($controller, $method, $params = null)
    {
//        $path = $_SERVER["REQUEST_SCHEME"];
//        $path .= '://';
//        $path .= $_SERVER["SERVER_NAME"];
//        $path .= $_SERVER["SCRIPT_NAME"];
//        $path .= '?page=';
//        $path .= $page;
//        if ($params != null)
//            foreach ($params as $key => $value)
//            {
//                $path .= '&';
//                $path .= $key;
//                $path .= '=';
//                $path .= $value;
//            }
//        return $path;

        $path = $_SERVER["REQUEST_SCHEME"];
        $path .= "://";
        $path .= $_SERVER["SERVER_NAME"];
        $path .= $_SERVER["SCRIPT_NAME"];
        $path .= "?controller=";
        $path .= $controller;
        $path .= "&method=";
        $path .= $method;
        if ($params != null){
            foreach ($params as $key => $value){
                $path .= "&";
                $path .= $key;
                $path .= "=";
                $path .= $value;
            }
        }
        return $path;
    }
}