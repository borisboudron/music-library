<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 19-09-18
 * Time: 11:01
 */

namespace MusicLibrary\Controllers;

use MusicLibrary\Models\Track;
use MusicLibrary\Models\TrackArtists;
use MusicLibrary\Repositories\ArtistRepository;
use MusicLibrary\Repositories\TrackArtistsRepository;
use MusicLibrary\Repositories\TrackRepository;
use MusicLibrary\Utils\TwigExtension;

class TrackController
{
    private $twig;
    public function __construct($twig)
    {
        $this->twig = $twig;
    }

    public function index()
    {
        $repo = new TrackRepository();
        $list = $repo->getAll();
        var_dump($list);
        foreach ($list as $track)
            $track->getTrackArtists();
        var_dump($list);
        return $this->twig->render("trackList.html.twig", [
            "tracks" => $list
        ]);
    }

    public function details($id)
    {
        $repo = new TrackRepository();
        $prod = $repo->getByID($id);
        return $this->twig->render("trackDetails.html.twig", [
            "prod" => $prod
        ]);
    }
    public function add($post = null)
    {
        $artistrepo = new ArtistRepository();
        $artists = $artistrepo->getAll();
        if (isset($post["submit"]))
        {
            //hydratation
            $track = new Track();
            $track->setTitle($post["title"]);
            $track->setArtists($post["artists"]);
            $track->setReleaseDate($post["release"]);
            $track->setGenre($post["genre"]);
            $track->setTags($post["tags"]);
            //si le formulaire est valide
            $isValid = true;
            $errors = [];
            if (strlen(trim($post["title"])) == 0)
            {
                $isValid = false;
                $errors["trackTitle"] = "Le champ est requis";
            }
            if (strlen(trim($post["artists"])) == 0)
            {
                $isValid = false;
                $errors["trackArtists"] = "Le champ est requis";
            }
            if (strlen(trim($post["release"])) == 0)
            {
                $isValid = false;
                $errors["trackRelease"] = "Le champ est requis";
            }
            if ($isValid)
            {
                $trackrepo = new TrackRepository();
                $trackartistsrepo = new TrackArtistsRepository();
//              $trackrepo = new TrackRepository();
                $trackId = $trackrepo->insert($track);
                $artistArray = explode(";", $post["artists"]);
                if ($trackId)
                {
                    foreach ($artistArray as $artist)
                    {
                        //hydratation
                        $trackartist = new TrackArtists();
                        $trackartist->setTrackId($trackId);
                        $artistrepo = new ArtistRepository();
                        $artistId = $artistrepo->insert($artist);
                        if (1)
                            $trackartist->setArtistId($post["artists"]);
                    }
                    if ($trackrepo->insert($track))
                        header("Location:" . TwigExtension::path('track', 'index'));
                }
            }
            else
            {
                return $this->twig->render("newTrack.html.twig", [
                    "track" => $track,
                    "artists" => $artists,
                    "errors" => $errors
                ]);
            }
        }
        else
            return $this->twig->render("newTrack.html.twig", [
                "artists" => $artists,
            ]);
    }
    public function update($id, $post = null)
    {
        $repo = new TrackRepository();
        $track = $repo->getByID($id);
        $track->setId($id);
        $errors = [];
        $errors['test'] = "Test";
        if (isset($post["submit"]))
        {
            //hydratation
            $track->setTitle($post["title"]);
            $track->setArtists($post["artists"]);
            $track->setReleaseDate($post["release"]);
            $track->setGenre($post["genre"]);
            $track->setTags($post["tags"]);
            //si le formulaire est valide
            $isValid = true;
            $errors = [];
            if (strlen(trim($post["title"])) == 0)
            {
                $isValid = false;
                $errors["trackTitle"] = "Le champ est requis";
            }
            if (strlen(trim($post["artists"])) == 0)
            {
                $isValid = false;
                $errors["trackArtists"] = "Le champ est requis";
            }
            if (strlen(trim($post["release"])) == 0)
            {
                $isValid = false;
                $errors["trackRelease"] = "Le champ est requis";
            }
            if ($isValid)
            {
                $repo = new TrackRepository();
                if($repo->update($track))
                    header("Location:" . TwigExtension::path('track', 'index'));
            }
            else
            {
                return $this->twig->render("newTrack.html.twig", [
                    "track" => $track,
                    "errors" => $errors,
                ]);
            }
        }
        else
            return $this->twig->render("newTrack.html.twig", [
                "track" => $track,
                "errors" => $errors,
            ]);
    }

    public function delete($id)
    {
        $repo = new TrackRepository();
        if ($repo->delete($id))
            header("Location:" . TwigExtension::path('track', 'index'));
    }
}