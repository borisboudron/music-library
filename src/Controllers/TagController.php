<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 20-09-18
 * Time: 08:43
 */

namespace MusicLibrary\Controllers;


use MusicLibrary\Models\Tag;
use MusicLibrary\Repositories\TagRepository;
use MusicLibrary\Utils\TwigExtension;

class TagController
{
    private $twig;

    public function __construct($twig)
    {
        $this->twig = $twig;
    }

    public function index()
    {
        $repo = new TagRepository();
        $tags = $repo->getAll();
        $this->twig->render("tagList.html.twig", [
            "tags" => $tags,
        ]);
    }

    public function details()
    {
        
    }
    public function add($post = null)
    {
        if (isset($post['submit']))
        {
            $tag = new Tag();
            $tag->setName($post['name']);
            $isValid = true;
            // Ajouter les validations
            if ($isValid)
            {
                $repo = new TagRepository();
                if ($repo->insert($tag))
                    header("Location:" . TwigExtension::path('track', 'trackList'));
            }
        }
        else
            return $this->twig->render('newTag.html.twig');
    }
    public function update()
    {

    }
    public function delete()
    {

    }
}