<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 20-09-18
 * Time: 13:27
 */

namespace MusicLibrary\Controllers;


use MusicLibrary\Repositories\ArtistRepository;
use MusicLibrary\Repositories\TrackRepository;

class TrackArtistsController
{
    private $twig;

    public function __construct($twig)
    {
        $this->twig = $twig;
    }

    public function index()
    {

    }

    public function details()
    {

    }

    public function add($post = null)
    {
        $trackrepo = new TrackRepository();
        $artistrepo = new ArtistRepository();
        $tracks = $trackrepo->getAll();
        $artists = $artistrepo->getAll();
        if (isset($post['submit']))
        {
            // Traitement
        }
        else
        {
            // Afficher la vue
            return $this->twig->render("newTrackArtists.html.twig", [
                "tracks" => $tracks,
                "artists" => $artists,
            ]);
        }
    }
}