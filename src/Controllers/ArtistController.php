<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 20-09-18
 * Time: 08:41
 */

namespace MusicLibrary\Controllers;


use MusicLibrary\Models\Artist;
use MusicLibrary\Repositories\ArtistRepository;
use MusicLibrary\Utils\TwigExtension;

class ArtistController
{
    private $twig;

    public function __construct($twig)
    {
        $this->twig = $twig;
    }

    public function index()
    {
        $repo = new ArtistRepository();
        $artists = $repo->getAll();
        $this->twig->render("artistList.html.twig", [
            "artists" => $artists,
        ]);
    }

    public function details()
    {
        
    }
    public function add($post = null)
    {
        if (isset($post['submit']))
        {
            $artist = new Artist();
            $artist->setName($post['name']);
            $isValid = true;
            // Ajouter les validations
            if ($isValid)
            {
                $repo = new ArtistRepository();
                if ($repo->insert($artist))
                    header("Location:" . TwigExtension::path('track', 'trackList'));
            }
        }
        else
            return $this->twig->render('newArtist.html.twig');
    }
    public function update()
    {

    }
    public function delete()
    {

    }
}