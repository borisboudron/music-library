<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 12-09-18
 * Time: 10:38
 */

namespace Toolbox;
use PDO;


abstract class BaseRepository
{
    protected $pdo;
    public function __construct()
    {
        $this->pdo = new PDO('mysql:host=localhost;dbname=musiclibrary;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    }
    protected abstract function getTableName();
    protected abstract function getPKBinding();
    protected abstract function getEntityName();
    protected abstract function getBindings();

    public function insert($item)
    {
        $bindings = $this->getBindings();

        $query = 'INSERT INTO ';
        $query .= $this->getTableName();
        $query .= '(';
        $query .= implode(',', array_keys($bindings));
        $query .= ') VALUES (:';
        $query .= implode(',:', array_keys($bindings));
        $query .= ')';
        $response = $this->pdo->prepare($query);
        $tab = [];
        foreach ($bindings as $key => $value)
            $tab[":" . $key] = $item->{"get".ucwords($value)}();
        $response->execute($tab);
        return $this->pdo->lastInsertId();
    }
    public function getByID($id)
    {
        $query = 'SELECT ';
        foreach ($this->getPKBinding() as $key => $value)
        {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($this->getBindings() as $key => $value)
        {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM ';
        $query .= $this->getTableName();
        $query .= ' WHERE ';
        $query .= key($this->getPKBinding());
        $query .= ' = :id';
        $response = $this->pdo->prepare($query);
        $response->execute(array(
            ':id' => $id,
        ));
        $item = $response->fetchObject($this->getEntityName());
        return $item;
    }
    public function getAll()
    {
        $bindings = $this->getBindings();
        $items = [];
        $query = 'SELECT ';
        foreach ($this->getPKBinding() as $key => $value)
        {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($bindings as $key => $value)
        {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM ';
        $query .= $this->getTableName();
        var_dump($query);
        $response = $this->pdo->query($query);
        while ($item = $response->fetchObject($this->getEntityName()))
            $items[] = $item;
        return $items;
    }
    public function update($item)
    {
        $bindings = $this->getBindings();
        $pkBinding = $this->getPKBinding();
        $query = 'UPDATE ';
        $query .= $this->getTableName();
        $query .= ' SET ';
        foreach ($this->getBindings() as $key => $value)
        {
            $query .= $key;
            $query .= " = ";
            $query .= ':';
            $query .= $key;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' WHERE ';
        $query .= key($pkBinding);
        $query .= ' = :';
        $query .= key($pkBinding);
        $response = $this->pdo->prepare($query);
        $tab = [];
        foreach ($pkBinding as $key => $value)
            $tab[":" . $key] = $item->{"get".ucwords($value)}();
        foreach ($bindings as $key => $value)
            $tab[":" . $key] = $item->{"get".ucwords($value)}();
        return $response->execute($tab);
    }
    public function delete($itemId)
    {
        $query = 'DELETE FROM ';
        $query .= $this->getTableName();
        $query .= ' WHERE ';
        $query .= key($this->getPKBinding());
        $query .= ' = :id';
        $response = $this->pdo->prepare($query);
        return $response->execute(array(
            ':id' => $itemId,
        ));
    }
}