<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 19-09-18
 * Time: 14:42
 */

namespace Toolbox;

class Router
{
    public function handleRoute($twig, $get, $post)
    {
        if (isset($get["controller"]))
        {
            $controllerName = $get["controller"];
            $className = "\\MusicLibrary\\Controllers\\" . ucWords($controllerName) . "Controller";
            $controller = new $className($twig);
            if(isset($get["method"]))
                if(isset($get["id"]))
                    if(empty($post))
                        return $controller->{$get["method"]}($get["id"]);
                    else
                        return $controller->{$get["method"]}($get["id"], $post);
                else
                    if(empty($post))
                        return $controller->{$get["method"]}();
                    else
                        return $controller->{$get["method"]}($post);
        }
        else
        {
//            $controller = new TrackController($twig);
//            echo $controller->trackList();
        }

//        if (isset($_GET["page"]))
//        {
//            switch ($_GET["page"])
//            {
//                case "index":
//                    $controller = new TrackController($twig);
//                    echo $controller->trackList();
//                    break;
//                case "details":
//                    $id = $_GET["id"];
//                    $controller = new TrackController($twig);
//                    echo $controller->trackDetails($id);
//                    break;
//                case "add":
//                    $controller = new TrackController($twig);
//                    echo $controller->newTrack($_POST);
//                    break;
//                case "update":
//                    var_dump($_POST);
//                    $id = $_POST["updateid"];
//                    $controller = new TrackController($twig);
//                    echo $controller->updateTrack($_POST, $id);
//                    break;
//            }
//        }
//        else
//        {
//            $controller = new TrackController($twig);
//            echo $controller->trackList();
//        }
    }
}