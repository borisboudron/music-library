<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 18-09-18
 * Time: 16:25
 */

namespace Toolbox;


abstract class SimpleObjectRepository extends BaseRepository
{
    public function getList($trackId)
    {
        $tags = [];
        $tablename = $this->getTableName();
        $query = 'SELECT ';
        $query .= $tablename;
        $query .= '.';
        foreach ($this->getPKBinding() as $key => $value)
        {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($this->getBindings() as $key => $value)
        {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM piste';
        $query .= $tablename;
        $query .= 's INNER JOIN ';
        $query .= $tablename;
        $query .= ' ON piste';
        $query .= $tablename;
        $query .= 's.';
        $query .= ucfirst($tablename);
        $query .= 'Id = ';
        $query .= $tablename;
        $query .= '.Id WHERE PisteId = :id';
        $response = $this->pdo->prepare($query);
        $response->execute(array(
            ':id' => $trackId,
        ));
        while ($tag = $response->fetchObject($this->getEntityName()))
            $tags[] = $tag->getName();
        return implode('; ', $tags);
    }
}