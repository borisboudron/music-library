<?php

use MusicLibrary\Repositories\TrackRepository;
use Toolbox\Router;

require_once "../vendor/autoload.php";
$loader = new Twig_Loader_Filesystem('../src/Views');
$twig = new Twig_Environment($loader);

$twigf = new Twig_Function("path", '\MusicLibrary\Utils\TwigExtension::path');
$twig->addFunction($twigf);

$repo = new TrackRepository();
$tracks = $repo->getAll();

$router = new Router();
echo $router->handleRoute($twig, $_GET, $_POST);